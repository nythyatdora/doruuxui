import React, { Component } from 'react';

import { ApplicationProvider, Layout, Text } from '@ui-kitten/components';
import { mapping, light as lightTheme } from '@eva-design/eva';

import StorybookUIRoot from './storybook/index';

export default class App extends Component {
  render() {
    return (
      <ApplicationProvider mapping={mapping} theme={lightTheme}>
        <StorybookUIRoot />
      </ApplicationProvider>
    );
  }
}
