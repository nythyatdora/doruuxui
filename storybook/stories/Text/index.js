import React from 'react';
import { Text } from '@ui-kitten/components';
import CenterView from '../CenterView/index';

import { storiesOf } from '@storybook/react-native';

import {
  withKnobs,
  text,
  number,
  boolean,
  color,
  select,
  radios,
  array,
  date,
  object,
} from '@storybook/addon-knobs';

storiesOf('Text', module)
  .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .addDecorator(withKnobs)
  .add('Heading 1', () => (
    <Text category='h1'>{text("title", "Heading 1")}</Text>
  ))
  .add('Heading 2', () => (
    <Text category='h2'>{text("title", "Heading 2")}</Text>
  ))
  .add('Heading 3', () => (
    <Text category='h3'>{text("title", "Heading 3")}</Text>
  ))
  .add('Heading 4', () => (
    <Text category='h4'>{text("title", "Heading 4")}</Text>
  ))
  .add('Heading 5', () => (
    <Text category='h5'>{text("title", "Heading 5")}</Text>
  ))
  .add('Heading 6', () => (
    <Text category='h6'>{text("title", "Heading 6")}</Text>
  ));